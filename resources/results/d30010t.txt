./resources/generated_summaries/d30010t/size_3_thresh_1.txt Scores
1-gram scores: 
F-1 Score: 0.25447163458265987
Precision: 0.30263157894736836
Recall   : 0.22006751418516124
2-gram scores: 
F-1 Score: 0.05752289885401932
Precision: 0.0684931506849315
Recall   : 0.049638628914938025
longest-gram scores: 
F-1 Score: 0.2069437126219616
Precision: 0.25877192982456143
Recall   : 0.18794660314268158

./resources/generated_summaries/d30010t/size_3_thresh_3.txt Scores
1-gram scores: 
F-1 Score: 0.2912699597536776
Precision: 0.2755681818181818
Recall   : 0.30973947983751904
2-gram scores: 
F-1 Score: 0.06485913865768397
Precision: 0.05894308943089431
Recall   : 0.07221030526175119
longest-gram scores: 
F-1 Score: 0.26185447394205996
Precision: 0.25
Recall   : 0.2810712889144262

./resources/generated_summaries/d30010t/size_3_thresh_8.txt Scores
1-gram scores: 
F-1 Score: 0.2627363813820258
Precision: 0.28308823529411764
Recall   : 0.2456592608553393
2-gram scores: 
F-1 Score: 0.049755809719731287
Precision: 0.054216867469879526
Recall   : 0.04601866877999085
longest-gram scores: 
F-1 Score: 0.2116583949323585
Precision: 0.23161764705882354
Recall   : 0.19993216659883328

./resources/generated_summaries/d30010t/size_3_thresh_6.txt Scores
1-gram scores: 
F-1 Score: 0.2627363813820258
Precision: 0.28308823529411764
Recall   : 0.2456592608553393
2-gram scores: 
F-1 Score: 0.049755809719731287
Precision: 0.054216867469879526
Recall   : 0.04601866877999085
longest-gram scores: 
F-1 Score: 0.2116583949323585
Precision: 0.23161764705882354
Recall   : 0.19993216659883328

./resources/generated_summaries/d30010t/size_3_thresh_99.txt Scores
1-gram scores: 
F-1 Score: 0.2627363813820258
Precision: 0.28308823529411764
Recall   : 0.2456592608553393
2-gram scores: 
F-1 Score: 0.049755809719731287
Precision: 0.054216867469879526
Recall   : 0.04601866877999085
longest-gram scores: 
F-1 Score: 0.2116583949323585
Precision: 0.23161764705882354
Recall   : 0.19993216659883328

./resources/generated_summaries/d30010t/size_3_no_threshold.txt Scores
1-gram scores: 
F-1 Score: 0.2661559289954461
Precision: 0.2777777777777778
Recall   : 0.25616904881610764
2-gram scores: 
F-1 Score: 0.057972283828349236
Precision: 0.0586734693877551
Recall   : 0.05737346356699727
longest-gram scores: 
F-1 Score: 0.2216961961784753
Precision: 0.2326388888888889
Recall   : 0.2147888785143687

./resources/generated_summaries/d30010t/random_summary.txt Scores
1-gram scores: 
F-1 Score: 0.12084159753032729
Precision: 0.17682926829268295
Recall   : 0.09193999537136793
2-gram scores: 
F-1 Score: 0.006839400248466758
Precision: 0.010638297872340425
Recall   : 0.005046662609048489
longest-gram scores: 
F-1 Score: 0.09549813249217536
Precision: 0.16463414634146342
Recall   : 0.08579369069565147

