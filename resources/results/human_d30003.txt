./resources/human_summaries/D30003.M.100.T.F Scores
1-gram scores: 
F-1 Score: 0.3140300987758308
Precision: 0.3111111111111111
Recall   : 0.3181572295496346
2-gram scores: 
F-1 Score: 0.05259817531420738
Precision: 0.05494505494505495
Recall   : 0.05048130551538938
longest-gram scores: 
F-1 Score: 0.29009128547618884
Precision: 0.2888888888888889
Recall   : 0.2945128008419147

./resources/human_summaries/D30003.M.100.T.B Scores
1-gram scores: 
F-1 Score: 0.35101084082007555
Precision: 0.3375527426160338
Recall   : 0.3666200466200466
2-gram scores: 
F-1 Score: 0.04741173855519245
Precision: 0.04666666666666667
Recall   : 0.048253887543290554
longest-gram scores: 
F-1 Score: 0.3095142090057091
Precision: 0.2995780590717299
Recall   : 0.3248795648795649

./resources/human_summaries/D30003.M.100.T.C Scores
1-gram scores: 
F-1 Score: 0.36281010888736304
Precision: 0.39393939393939387
Recall   : 0.3363172130260738
2-gram scores: 
F-1 Score: 0.05206828601880231
Precision: 0.05319148936170213
Recall   : 0.05106582737650699
longest-gram scores: 
F-1 Score: 0.33056811450813434
Precision: 0.3636363636363636
Recall   : 0.3102174618630315

./resources/human_summaries/D30003.M.100.T.A Scores
1-gram scores: 
F-1 Score: 0.3432361217347337
Precision: 0.3333333333333333
Recall   : 0.35484209180411713
2-gram scores: 
F-1 Score: 0.06062755373670651
Precision: 0.05825242718446602
Recall   : 0.06325461772270281
longest-gram scores: 
F-1 Score: 0.32363125327865666
Precision: 0.3162393162393163
Recall   : 0.33547116736990157

Average of all of them Scores1-gram scores: 
F-1 Score: 0.3427717925545008
Precision: 0.343984145249968
Recall   : 0.343984145249968
2-gram scores: 
F-1 Score: 0.05317643840622716
Precision: 0.053263909539472444
Recall   : 0.05326390953947244
longest-gram scores: 
F-1 Score: 0.31345121556717226
Precision: 0.31708565695907465
Recall   : 0.31627024873860315

