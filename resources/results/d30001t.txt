./resources/generated_summaries/d30001t/size_3_thresh_1.txt Scores
1-gram scores: 
F-1 Score: 0.2758834508394822
Precision: 0.3014705882352941
Recall   : 0.2543880964759601
2-gram scores: 
F-1 Score: 0.06156432562890302
Precision: 0.06686046511627908
Recall   : 0.057059933645964095
longest-gram scores: 
F-1 Score: 0.22584196061422918
Precision: 0.25
Recall   : 0.21141978796989083

./resources/generated_summaries/d30001t/size_3_thresh_3.txt Scores
1-gram scores: 
F-1 Score: 0.17374948742871882
Precision: 0.23913043478260868
Recall   : 0.13648308053979522
2-gram scores: 
F-1 Score: 0.02488716387206349
Precision: 0.03389830508474576
Recall   : 0.019663548968711628
longest-gram scores: 
F-1 Score: 0.14555823840880136
Precision: 0.22826086956521738
Recall   : 0.13022917184686214

./resources/generated_summaries/d30001t/size_3_thresh_8.txt Scores
1-gram scores: 
F-1 Score: 0.17603630880199825
Precision: 0.22169811320754718
Recall   : 0.14601558202792408
2-gram scores: 
F-1 Score: 0.024376264158663327
Precision: 0.03125
Recall   : 0.019985136095565417
longest-gram scores: 
F-1 Score: 0.14187650158963427
Precision: 0.19339622641509435
Recall   : 0.12725385594912483

./resources/generated_summaries/d30001t/size_3_thresh_6.txt Scores
1-gram scores: 
F-1 Score: 0.2618131818151737
Precision: 0.266025641025641
Recall   : 0.25783245552566736
2-gram scores: 
F-1 Score: 0.054132993224854514
Precision: 0.053398058252427175
Recall   : 0.05490190173556095
longest-gram scores: 
F-1 Score: 0.21105833119029024
Precision: 0.21474358974358973
Recall   : 0.2079481151623379

./resources/generated_summaries/d30001t/size_3_thresh_99.txt Scores
1-gram scores: 
F-1 Score: 0.17603630880199825
Precision: 0.22169811320754718
Recall   : 0.14601558202792408
2-gram scores: 
F-1 Score: 0.024376264158663327
Precision: 0.03125
Recall   : 0.019985136095565417
longest-gram scores: 
F-1 Score: 0.14187650158963427
Precision: 0.19339622641509435
Recall   : 0.12725385594912483

./resources/generated_summaries/d30001t/size_3_no_threshold.txt Scores
1-gram scores: 
F-1 Score: 0.2855498022662346
Precision: 0.2922077922077922
Recall   : 0.2792935343625911
2-gram scores: 
F-1 Score: 0.061560810823462625
Precision: 0.06127450980392157
Recall   : 0.061867625953656406
longest-gram scores: 
F-1 Score: 0.24417766832239862
Precision: 0.24999999999999997
Recall   : 0.2393372740492929

./resources/generated_summaries/d30001t/random_summary.txt Scores
1-gram scores: 
F-1 Score: 0.2612019875151823
Precision: 0.22706422018348627
Recall   : 0.30756044817167355
2-gram scores: 
F-1 Score: 0.06125509234986756
Precision: 0.052083333333333336
Recall   : 0.07437114221164476
longest-gram scores: 
F-1 Score: 0.21727953498426414
Precision: 0.1972477064220184
Recall   : 0.2676409201534091

