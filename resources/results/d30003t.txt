./resources/generated_summaries/d30003t/size_3_thresh_1.txt Scores
1-gram scores: 
F-1 Score: 0.26744992004071994
Precision: 0.3136363636363636
Recall   : 0.2337046708565696
2-gram scores: 
F-1 Score: 0.03293408956072005
Precision: 0.03985507246376811
Recall   : 0.028079425738483783
longest-gram scores: 
F-1 Score: 0.2364747564203708
Precision: 0.2909090909090909
Recall   : 0.21538859874302912

./resources/generated_summaries/d30003t/size_3_thresh_3.txt Scores
1-gram scores: 
F-1 Score: 0.29180650907818184
Precision: 0.35784313725490197
Recall   : 0.2469097990616978
2-gram scores: 
F-1 Score: 0.07136016206907045
Precision: 0.08984375
Recall   : 0.059227091850516764
longest-gram scores: 
F-1 Score: 0.25477834631509366
Precision: 0.3333333333333333
Recall   : 0.2299167920686908

./resources/generated_summaries/d30003t/size_3_thresh_8.txt Scores
1-gram scores: 
F-1 Score: 0.2600965887531376
Precision: 0.34444444444444444
Recall   : 0.20935646631849164
2-gram scores: 
F-1 Score: 0.03971102340261077
Precision: 0.05454545454545454
Recall   : 0.03124928779132456
longest-gram scores: 
F-1 Score: 0.21799880316267334
Precision: 0.3222222222222222
Recall   : 0.19524224720427252

./resources/generated_summaries/d30003t/size_3_thresh_6.txt Scores
1-gram scores: 
F-1 Score: 0.26138069462493074
Precision: 0.42424242424242425
Recall   : 0.18919890236345932
2-gram scores: 
F-1 Score: 0.06963771259705603
Precision: 0.11875000000000001
Recall   : 0.049285963984373404
longest-gram scores: 
F-1 Score: 0.1931112174867533
Precision: 0.393939393939394
Recall   : 0.17541102357558053

./resources/generated_summaries/d30003t/size_3_thresh_99.txt Scores
1-gram scores: 
F-1 Score: 0.2600965887531376
Precision: 0.34444444444444444
Recall   : 0.20935646631849164
2-gram scores: 
F-1 Score: 0.03971102340261077
Precision: 0.05454545454545454
Recall   : 0.03124928779132456
longest-gram scores: 
F-1 Score: 0.21799880316267334
Precision: 0.3222222222222222
Recall   : 0.19524224720427252

./resources/generated_summaries/d30003t/size_3_no_threshold.txt Scores
1-gram scores: 
F-1 Score: 0.22859317603651702
Precision: 0.2389705882352941
Recall   : 0.21964223540172906
2-gram scores: 
F-1 Score: 0.03886952919964988
Precision: 0.04216867469879518
Recall   : 0.03608879397817634
longest-gram scores: 
F-1 Score: 0.20638217986967955
Precision: 0.21691176470588233
Recall   : 0.1996534478180048

./resources/generated_summaries/d30003t/random_summary.txt Scores
1-gram scores: 
F-1 Score: 0.1695303762229391
Precision: 0.1791044776119403
Recall   : 0.16130801687763713
2-gram scores: 
F-1 Score: 0.014364074543573595
Precision: 0.015822784810126583
Recall   : 0.013168942707777659
longest-gram scores: 
F-1 Score: 0.14775316057263013
Precision: 0.15671641791044777
Recall   : 0.14207075624797144

