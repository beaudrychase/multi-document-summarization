import pandas as pd
import os

def read_documents_raw():
    docs = []
    current_dir_path = os.getcwd()
    target = os.path.join(current_dir_path,"resources","original_documents")

    for _, directory, file in os.walk(target):
        for di in directory:
            sub_path = os.path.join(target, di)
            for document_file in os.listdir(sub_path):
                path_to_document_file = os.path.join(sub_path, document_file)
                if os.path.isfile(path_to_document_file) and document_file[0] != ".":
                    with open(path_to_document_file, encoding="ascii") as document:
                        item = {"file_id":di[:-5], "contents":document.read()}
                        if item not in docs:
                            docs.append(item)
    return pd.DataFrame(docs)


def read_summaries_raw():
    docs = []
    current_dir_path = os.getcwd()
    target = os.path.join(current_dir_path,"resources","human_summaries")

    for document_file in os.listdir(target):
        path_to_document_file = os.path.join(target, document_file)
        if os.path.isfile(path_to_document_file) and document_file[0] != ".":
            with open(path_to_document_file, encoding="ascii") as document:
                item = {"file_id":document_file[:6].lower(), "contents":document.read()}
                docs.append(item)

    return pd.DataFrame(docs)
